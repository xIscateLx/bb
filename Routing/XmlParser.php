<?php
/**
 * Парсер Xml файлов.
 * Помогает пропарсит указанный XML и получить из него маршруты.
 */
namespace Routing;

use Routing\Entity\Route;

class XmlParser
{
    /**
     * @var string
     */
    protected $file;
    
    /**
     * @var array
     */
    protected $routes;
    
    /**
     * Конструктор класса
     * @return void
     */
    public function __construct()
    {
        $this->file = __DIR__.'/routing.xml';
        $this->registerRoutes($this->file);
    }
    /**
     * Парсим указанный файл и создаем роуты
     * @var string $file
     * @return void
     */
	private function registerRoutes($file)
	{	
		$xml = simplexml_load_file($file);
		$routes = array();
		foreach ($xml->route as $item)
		{
		    $route = $this->buildRoute($item);					
			$routes[$route->getId()] = $route; 
		}
        $result = array();
		$result = $this->uniqueRoute($routes, $result);
		$this->routes = $routes;
	}
    
	/**
     * собираем один маршрут.
     * 
     * @var SimpleXmlElement $item
     * @return Routing\Entity\Route $route
     */
	private function buildRoute($item)
	{
		$route = new Route((string)$item['id']);
		$route->setPath((string)$item['path']);
		
		foreach ($item->default as $default) {
			$route->addDefault((string)$item->default['key'], (string)$item->default);
		}
		if (!$route->getDefault('_controller')) {
			throw new \Exception(sprintf('The route %s must be specified controller', $item['id']));
		} else {
			$controllerName = $route->getDefault('_controller');
			$controller = explode(':', $controllerName);
			$class = $controller[0].'Controller';
			$method = $controller[1].'Action';
			
			if (count($controller) !== 2) throw new \Exception(sprintf("Unable to parse the controller %s", $controllerName));			
			
			if (!method_exists($class, $method)) throw new \Exception(sprintf("The specified class %s don't containing of this method %s", $class, $method));
			$route->setClass($class);
			$route->setAction($method);			
		}	
		
		if (!isset($item['methods'])) {				
			$route->addMethod('GET');
		} else {
			$methods = $item['methods'];
			$methods = explode('|', $methods);
			foreach ($methods as $method) {
				$route->addMethod((string)$method);
			}
		}
		
		return $route;
	}


    /**
     * @TODO разработать проверку на одинаковые маршруты.
     */
    private function uniqueRoute($routes, $result)
    {
        return $routes;
    }

    /**
     * возвращает все маршруты 
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }
}
