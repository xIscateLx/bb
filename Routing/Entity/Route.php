<?php
/**
 * класс для хранения маршрута
 */
namespace Routing\Entity;

class Route
{
	protected $id;
	
	protected $path;
	
	protected $methods;
	
	protected $defaults;
	
	protected $class;
	
	protected $action;
	
	public function __construct($id)
	{
		$this->id = $id;
	}
    
    public function setId($id)
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getId()
    {
        return $this->id;
    }
	
	public function setPath($path)
	{
		$this->path = $path;
		
		return $this;
	}
	
	public function getPath()
	{
		return $this->path;
	}
	
	public function addMethod($method)
	{
		$this->methods[] = $method;
		
		return $this;
	}
	
	public function getMethods()
	{
		return $this->methods;
	}
	
	public function addDefault($key, $option)
	{
		$this->defaults[$key] = $option;
		
		return $this;
	}
	
	public function getDefaults()
	{
		return $this->defaults;
	}
	
	public function getDefault($key)
	{
		return $this->defaults[$key];
	}
	
	public function setClass($class)
	{
		$this->class = $class;
		
		return $this;
	}
	
	public function getClass()
	{
		return $this->class;
	}
	
	public function setAction($action)
	{
		$this->action = $action;
		
		return $this;
	}
	
	public function getAction()
	{
		return $this->action;
	}
}
