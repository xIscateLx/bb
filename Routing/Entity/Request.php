<?php
/**
 * Класс для хранения информации о текущем запросе
 */
namespace Routing\Entity;

class Request
{
    /**
     * @var $_GET
     */
	public $query;
    
    /**
     * @var $_POST
     */
    public $request;
    
    /**
     * @var $_SERVER
     */
    public $server;
    
    /**
     * @var string
     */
    public $method;
	
    /**
     * @return void
     */
    public function __construct()
    {
        $this->query = $_GET;
        $this->request = $_POST;
        $this->server = $_SERVER;
        //var_dump($this->server);
        $this->method = $this->server['REQUEST_METHOD'];
    }  
    
    /**
     * возвращает uri текущего запроса
     * @var string
     */
    public function getUri()
    {
        $uri = $this->server['REQUEST_URI'];
        $uri = explode('?', $uri);
        return $uri[0];
    }
    
    /**
     * @var string
     */
    public function getMethod()
    {
        return $this->method;
    }  
}
