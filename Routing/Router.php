<?php
/**
 * Данный класс позволяет определить маршрут для текущего запроса.
 */
namespace Routing;

use Routing\Entity\Request;
use Routing\XmlParser;

class Router
{
    /**
     * @var Request
     */
    protected $request;
    
    /**
     * @var array 
     */
    protected $routers;
    
    /**
     * @var Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->init();        
    }
    
    /**
     * @return void
     */
    protected function init()
    {
        try {
            $parser = new XmlParser();
            $this->routers = $parser->getRoutes();
        } 
        catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    
    /**
     * Вызов контроллера для текущего запроса
     * 
     * @return void
     */
    public function send()
    {
        $route = $this->searchRoute();
        $request = $this->request;
        if (!$route) {
            //throw new \Exception(sprintf('No route found for "%s %s"', $request->getMethod(), $request->getUri()));
            echo sprintf('No route found for "%s %s"', $request->getMethod(), $request->getUri());
            return;
        }
        $class = $route->getClass();
        $action = $route->getAction();
        $controller = new $class();
        call_user_func_array(array($controller, $action), array($this->request));
    }
    
    /**
     * Поиск маршрута для текущего запроса
     * 
     * @return Routing\Entity\Route $item
     */
    private function searchRoute()
    {
        $request = $this->request;
        foreach ($this->routers as $name => $item) {
            if (!in_array($request->getMethod(), $item->getMethods())) {
                continue;
            }
            if ($request->getUri() == $item->getPath()) {
                return $item;
            }
        }
        return null;
    }
}