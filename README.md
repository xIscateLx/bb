# README #

Реализован простой роутер.

Список маршрутов берет из файла Routing\routing.xml

Тестировал на nginx со следующими настройками:

```
#!nginx

server {
	server_name bb.mobileunits.ru www.bb.mobileunits.ru;
	listen 192.168.1.200;
	root /home/bb/public_html;
	index index.html index.htm index.php;
	access_log /var/log/virtualmin/bb.mobileunits.ru_access_log combined;
	error_log /var/log/virtualmin/bb.mobileunits.ru_error_log error;
	fastcgi_param GATEWAY_INTERFACE CGI/1.1;
	fastcgi_param SERVER_SOFTWARE nginx;
	fastcgi_param QUERY_STRING $query_string;
	fastcgi_param REQUEST_METHOD $request_method;
	fastcgi_param CONTENT_TYPE $content_type;
	fastcgi_param CONTENT_LENGTH $content_length;
	fastcgi_param SCRIPT_FILENAME /home/bb/public_html$fastcgi_script_name;
	fastcgi_param SCRIPT_NAME $fastcgi_script_name;
	fastcgi_param REQUEST_URI $request_uri;
	fastcgi_param DOCUMENT_URI $document_uri;
	fastcgi_param DOCUMENT_ROOT /home/bb/public_html;
	fastcgi_param SERVER_PROTOCOL $server_protocol;
	fastcgi_param REMOTE_ADDR $remote_addr;
	fastcgi_param REMOTE_PORT $remote_port;
	fastcgi_param SERVER_ADDR $server_addr;
	fastcgi_param SERVER_PORT $server_port;
	fastcgi_param SERVER_NAME $server_name;
	fastcgi_param HTTPS $https;
	location ~ \.php$ {
		try_files $uri =404;
		fastcgi_pass unix:/var/php-nginx/145588868811210.sock/socket;
	}
    
    location / {
		try_files $uri /index.php?$query_string;
	}

}
```

Пример написания маршрутов:

```
#!xml
<?xml version='1.0' standalone='yes'?>

<routes>
    <route id="homepage" path="/" methods="GET">
        <default key="_controller">Main\Main:index</default>
    </route>    
    <route id="login" path="/login" methods="GET|HEAD">    
    	<default key="_controller">Main\Main:login</default>    
    </route>  
    <route id="loginPost" path="/login" methods="POST">    
        <default key="_controller">Main\Main:loginPost</default>
    </route>  
</routes>
```

Для каждого маршрута можна прописать несколько методов через "|"

Контроллер указывается через специальный ключ:

```
#!xml

<default key="_controller">Main\Main:loginPost</default>
```

где Main\Main - название класса контроллера. Класс указывается без окончания "Controller". Т.е. данный пример будет искать класс Main\MainController.
loginPost - название метода класса контроллера, на который придет запрос. Метод указывается так же, как и класс без окончания "Action"