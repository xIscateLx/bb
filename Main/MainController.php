<?php

namespace Main;

class MainController
{
	public function indexAction()
	{
		echo 'Main controller on the route "/"';
	}
    
    public function loginAction()
    {
        echo 'GET controller on the route "/login"';
    }
    
    public function loginPostAction()
    {
        echo 'POST controller on the route "/login"';
    }
}
