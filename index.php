<?php

use Routing\Entity\Request;
use Routing\Router;

// Подключаем автолоадер
require_once __DIR__ . '/autoload.php';

$request =  new Request();

$route = new Router($request);
$route->send();


