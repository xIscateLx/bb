<?php

spl_autoload_register(array('Autoloader', 'loadPackages'));

class AutoLoader
{
    private static $_lastLoadedFilename;
 
    public static function loadPackages($className)
    {
		$pathParts = explode('\\', $className);
		self::$_lastLoadedFilename = __DIR__ . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pathParts) . '.php';
		if (!file_exists(self::$_lastLoadedFilename)) {
			throw new \Exception(sprintf("Class %s not found", self::$_lastLoadedFilename));
		}
        require_once(self::$_lastLoadedFilename);
    }    
}